# Dados Climáticos
 
Este projeto é um exemplo de Crawler capaz de capturar dados referentes ao clima das capitais brasileiras. Quando executado o spider busca os dados no site que serve de base de leitura e armazena em uma base de dados PostgreSQL.
 
## Requisitos
- Python 3.8
- PostgreSQL
 
## Rodando o projeto
 
### Banco de dados
 
Após iniciar o banco de dados é necessário que a tabela onde os dados serão armazenados seja criada.
 
```sql
create table if not exists capital_climate (
   id serial primary key,
   city_name varchar(200),
   state varchar(2),  
   temperature numeric,
   pressure numeric,
   wind numeric,
   humidity numeric,
   created_at TIMESTAMP not null
);
```
 
Com tudo criado é necessário adicionar valor às variáveis de ambiente um arquivo chamado `.env` que deve ser criado na raiz da pasta tempo_agora_ws. Abaixo estão as variáveis a serem adicionadas, os valores de acesso devem ser preenchidos de acordo com o seu banco de dados.
 
```
DB_HOSTNAME=''
DB_USERNAME=''
DB_PASSWORD=''
DB_DATABASE=''
```
 
## Criando VirtualEnv
 
Com o python instalado na versão indicada acima, é necessário criar um ambiente virtual seguindo os comandos abaixo na pasta raíz do projeto.
 
```shell
python -m venv .venv
source .venv/bin/activate
```
Com o ambiente configurado é preciso instalar as dependências
 
```shell
pip install -r requirements.txt
```
 
## Executando o spider
 
Como o projeto é implementado em cima do template padrão do Scrapy ([Creating a project](https://docs.scrapy.org/en/latest/intro/tutorial.html#creating-a-project)), é possível usar os comandos do Scrapy CLi na linha de comando.
 
Para testar o spider basta executar o comando a seguir dentro da pasta `tempo_agora`.
 
```shell
# Estando na raiz /tempo_agora_ws
cd tempo_agora/
scrapy crawl tempo_agora
```
Após isso os dados estarão disponíveis para consulta no banco de dados.

começou as 23:55 do notebook