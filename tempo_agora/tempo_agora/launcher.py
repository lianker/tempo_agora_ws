from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
# from spiders.tempo_agora_spider import TempoAgoraSpider

def main(event={}, context={}):
    process = CrawlerProcess(get_project_settings())
    process.crawl('tempo_agora')
    process.start()

if __name__ == "__main__":
    main()