import psycopg2
from datetime import datetime


class DatabaseManager():
    def __init__(self, hostname, username, password, database):
        self.hostname = hostname
        self.username = username
        self.password = password
        self.database = database

    def open_connection(self):
        self.conn = psycopg2.connect(
            host=self.hostname,
            user=self.username,
            password=self.password,
            dbname=self.database
        )
        self.cur = self.conn.cursor()

    def close_connection(self):
        self.cur.close()
        self.conn.close()

    def queryCapitals(self):
        self.cur.execute("""select * from capital_climate""")
        rows = self.cur.fetchall()

        for row in rows:
            print(row)

    def insert_climate(self, climate_item):
        stmt = """insert into capital_climate(city_name,
        state, temperature, pressure, wind, humidity, created_at)
        values (%s, %s, %s, %s, %s, %s, %s)"""

        self.cur.execute(stmt, (
            climate_item['city_name'],
            climate_item['state'],
            climate_item['temperature'],
            climate_item['pressure'],
            climate_item['wind'],
            climate_item['humidity'],
            datetime.now()
        ))
        self.conn.commit()
