import scrapy
import json
import unidecode
from tempo_agora.items import TempoAgoraItem

CAPITAIS = [
    {"city": "Rio Branco", "uf": "AC"},
    {"city": "Maceió", "uf": "AL"},
    {"city": "Macapá", "uf": "AP"},
    {"city": "Manaus", "uf": "AM"},
    {"city": "Salvador", "uf": "BA"},
    {"city": "Fortaleza", "uf": "CE"},
    {"city": "Brasília", "uf": "DF"},
    {"city": "Vitória", "uf": "ES"},
    {"city": "Goiânia", "uf": "GO"},
    {"city": "São Luís", "uf": "MA"},
    {"city": "Cuiabá", "uf": "MT"},
    {"city": "Campo Grande", "uf": "MS"},
    {"city": "Belo Horizonte", "uf": "MG"},
    {"city": "Belém", "uf": "PA"},
    {"city": "João Pessoa", "uf": "PB"},
    {"city": "Curitiba", "uf": "PR"},
    {"city": "Recife", "uf": "PE"},
    {"city": "Teresina", "uf": "PI"},
    {"city": "Rio de Janeiro", "uf": "RJ"},
    {"city": "Natal", "uf": "RN"},
    {"city": "Porto Alegre", "uf": "RS"},
    {"city": "Porto Velho", "uf": "RO"},
    {"city": "Boa Vista", "uf": "RR"},
    {"city": "Florianópolis", "uf": "SC"},
    {"city": "São Paulo", "uf": "SP"},
    {"city": "Aracaju", "uf": "SE"},
    {"city": "Palmas", "uf": "TO"}
]


def gen_url(capital):
    city = unidecode.unidecode(capital['city']).replace(" ", "")
    return f"https://www.tempoagora.com.br/previsao-do-tempo/{capital['uf']}/{city}"


class TempoAgoraSpider(scrapy.Spider):
    name = 'tempo_agora'
    start_urls = map(gen_url, CAPITAIS)

    def parse(self, response):        
        crawled_item = response.xpath(
            '//script[@type="text/javascript"]/text()')[-1].get()
        general_data = json.loads(crawled_item[16:-1])
        weather_data = general_data['data'][0]['weatherData']
        climateItem = TempoAgoraItem(
            city_name = weather_data['city'],
            state = weather_data['state'],
            temperature = weather_data['temp'],
            pressure = weather_data['pressure'],
            wind = weather_data['wind'],
            humidity = weather_data['humidity']
        )
        yield climateItem