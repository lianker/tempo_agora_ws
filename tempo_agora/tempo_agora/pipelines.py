# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from tempo_agora.database import DatabaseManager


class TempoAgoraPipeline:
    def process_item(self, item, spider):
        return item


class DataBasePipeline:
    def __init__(self, hostname, username, password, database):
        self.hostname = hostname
        self.username = username
        self.password = password
        self.database = database

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            hostname=crawler.settings.get('DB_HOSTNAME'),
            username=crawler.settings.get('DB_USERNAME'),
            password=crawler.settings.get('DB_PASSWORD'),
            database=crawler.settings.get('DB_DATABASE')
        )

    def open_spider(self, spider):
        self.db = DatabaseManager(
            self.hostname,
            self.username,
            self.password,
            self.database
        )
        self.db.open_connection()

    def close_spider(self, spider):
        self.db.close_connection()

    def process_item(self, item, spider):
        self.db.insert_climate(item)
        return item
