# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class TempoAgoraItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    city_name = scrapy.Field()
    state = scrapy.Field()
    temperature = scrapy.Field()
    pressure = scrapy.Field()
    wind = scrapy.Field()
    humidity = scrapy.Field()
